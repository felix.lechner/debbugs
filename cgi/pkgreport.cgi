#!/usr/bin/perl -wT
# This script is part of debbugs, and is released
# under the terms of the GPL version 2, or any later
# version at your option.
# See the file README and COPYING for more information.
#
# [Other people have contributed to this file; their copyrights should
# go here too.]
# Copyright 2004-2006 by Anthony Towns <ajt@debian.org>
# Copyright 2007 by Don Armstrong <don@donarmstrong.com>.


use warnings;
use strict;

use POSIX qw(strftime nice);

use Debbugs::Config qw(:globals :text :config);

use Debbugs::User;

use Debbugs::Common qw(getparsedaddrs make_list getmaintainers getpseudodesc hide_address);

use Debbugs::Bugs qw(get_bugs bug_filter newest_bug);
use Debbugs::Packages qw(getsrcpkgs getpkgsrc get_versions);

use Debbugs::Status qw(splitpackages);

use Debbugs::CGI qw(:all);

use Debbugs::CGI::Pkgreport qw(:all);

use Debbugs::Text qw(:templates);

use CGI::Simple;
my $q = new CGI::Simple;

if ($q->request_method() eq 'HEAD') {
     print $q->header(-type => "text/html",
		      -charset => 'utf-8',
		     );
     exit 0;
}

my $default_params = {ordering => 'normal',
		      archive  => 0,
		      repeatmerged => 0,
		      include      => [],
		      exclude      => [],
		     };

our %param = cgi_parameters(query => $q,
			    single => [qw(ordering archive repeatmerged),
				       qw(bug-rev pend-rev sev-rev),
				       qw(maxdays mindays version),
				       qw(data which dist newest),
				       qw(maxbug minbug page max-bugs),
				       qw(base-order),
				      ],
			    default => $default_params,
			   );

my ($form_options,$param) = ({},undef);
($form_options,$param)= form_options_and_normal_param(\%param)
     if $param{form_options};

%param = %{$param} if defined $param;

if (exists $param{form_options} and defined $param{form_options}) {
     delete $param{form_options};
     delete $param{submit} if exists $param{submit};
     for my $default (keys %{$default_params}) {
	  if (exists $param{$default} and
	      not ref($default_params->{$default}) and
	      $default_params->{$default} eq $param{$default}
	     ) {
	       delete $param{$default};
	  }
     }
     for my $incexc (qw(include exclude)) {
	  next unless exists $param{$incexc};
	  $param{$incexc} = [grep /\S\:\S/, make_list($param{$incexc})];
     }
     for my $key (keys %package_search_keys) {
	  next unless exists $param{key};
	  $param{$key} = [map {split /\s*,\s*/} make_list($param{$key})];
     }
     # kill off keys for which empty values are meaningless
     for my $key (qw(package src submitter affects severity status dist msgid)) {
	  next unless exists $param{$key};
	  $param{$key} = [grep {defined $_ and length $_}
			  make_list($param{$key})];
     }
     print $q->redirect(munge_url('pkgreport.cgi?',%param));
     exit 0;
}

# map from yes|no to 1|0
for my $key (qw(repeatmerged bug-rev pend-rev sev-rev base-order)) {
     if (exists $param{$key}){
	  if ($param{$key} =~ /^no$/i) {
	       $param{$key} = 0;
	  }
	  elsif ($param{$key}) {
	       $param{$key} = 1;
	  }
     }
}

if (lc($param{archive}) eq 'no') {
     $param{archive} = 0;
}
elsif (lc($param{archive}) eq 'yes') {
     $param{archive} = 1;
}

# fixup dist
if (exists $param{dist} and $param{dist} eq '') {
     delete $param{dist};
}

my $include = $param{'&include'} || $param{'include'} || "";
my $exclude = $param{'&exclude'} || $param{'exclude'} || "";

my $users = $param{'users'} || "";

my $ordering = $param{'ordering'};
my $raw_sort = ($param{'raw'} || "no") eq "yes";
my $old_view = ($param{'oldview'} || "no") eq "yes";
my $age_sort = ($param{'age'} || "no") eq "yes";
unless (defined $ordering) {
   $ordering = "normal";
   $ordering = "oldview" if $old_view;
   $ordering = "raw" if $raw_sort;
   $ordering = 'age' if $age_sort;
}
$param{ordering} = $ordering;

our ($bug_order) = $ordering =~ /(age(?:rev)?)/;
$bug_order = '' if not defined $bug_order;

## yes/no etc have already been mapped to 1/0 above.
my $bug_rev = $param{'bug-rev'} || 0;
my $pend_rev = $param{'pend-rev'} || 0;
my $sev_rev = $param{'sev-rev'} || 0;

my @inc_exc_mapping = ({name   => 'pending',
			incexc => 'include',
			key    => 'pend-inc',
		       },
		       {name   => 'pending',
			incexc => 'exclude',
			key    => 'pend-exc',
		       },
		       {name   => 'severity',
			incexc => 'include',
			key    => 'sev-inc',
		       },
		       {name   => 'severity',
			incexc => 'exclude',
			key    => 'sev-exc',
		       },
		       {name   => 'subject',
			incexc => 'include',
			key    => 'includesubj',
		       },
		       {name   => 'subject',
			incexc => 'exclude',
			key    => 'excludesubj',
		       },
		      );
for my $incexcmap (@inc_exc_mapping) {
     push @{$param{$incexcmap->{incexc}}}, map {"$incexcmap->{name}:$_"}
	  map{split /\s*,\s*/} make_list($param{$incexcmap->{key}})
	       if exists $param{$incexcmap->{key}};
     delete $param{$incexcmap->{key}};
}


my $maxdays = ($param{'maxdays'} || -1);
my $mindays = ($param{'mindays'} || 0);
my $version = $param{'version'} || undef;


our %hidden = map { $_, 1 } qw(status severity classification);
our %cats = (
    "status" => [ {
        "nam" => "Status",
        "pri" => [map { "pending=$_" }
            qw(pending forwarded pending-fixed fixed done absent)],
        "ttl" => ["Outstanding","Forwarded","Pending Upload",
                  "Fixed in NMU","Resolved","From other Branch"],
        "def" => "Unknown Pending Status",
        "ord" => [0,1,2,3,4,5,6],
    } ],
    "severity" => [ {
        "nam" => "Severity",
        "pri" => [map { "severity=$_" } @gSeverityList],
        "ttl" => [map { $gSeverityDisplay{$_} } @gSeverityList],
        "def" => "Unknown Severity",
        "ord" => [0..@gSeverityList],
    } ],
    "classification" => [ {
        "nam" => "Classification",
        "pri" => [qw(pending=pending+tag=wontfix 
                     pending=pending+tag=moreinfo
                     pending=pending+tag=patch
                     pending=pending+tag=confirmed
                     pending=pending)],
        "ttl" => ["Will Not Fix","More information needed",
                  "Patch Available","Confirmed"],
        "def" => "Unclassified",
        "ord" => [2,3,4,1,0,5],
    } ],
    "oldview" => [ qw(status severity) ],
    "normal" => [ qw(status severity classification) ],
);

if (exists $param{which} and exists $param{data}) {
     $param{$param{which}} = [exists $param{$param{which}}?(make_list($param{$param{which}})):(),
			      make_list($param{data}),
			     ];
     delete $param{which};
     delete $param{data};
}

if (defined $param{maintenc}) {
     $param{maint} = maint_decode($param{maintenc});
     delete $param{maintenc}
}

if (exists $param{pkg}) {
     $param{package} = $param{pkg};
     delete $param{pkg};
}

if (not grep {exists $param{$_}} keys %package_search_keys and exists $param{users}) {
     $param{usertag} = [make_list($param{users})];
}

my %bugusertags;
my %ut;
my %seen_users;

for my $user (map {split /[\s*,\s*]+/} make_list($param{users}||[])) {
    next unless length($user);
    add_user($user,\%ut,\%bugusertags,\%seen_users,\%cats,\%hidden);
}

if (defined $param{usertag}) {
     for my $usertag (make_list($param{usertag})) {
	  my %select_ut = ();
	  my ($u, $t) = split /:/, $usertag, 2;
	  Debbugs::User::read_usertags(\%select_ut, $u);
	  unless (defined $t && $t ne "") {
	       $t = join(",", keys(%select_ut));
	  }
	  add_user($u,\%ut,\%bugusertags,\%seen_users,\%cats,\%hidden);
	  push @{$param{tag}}, split /,/, $t;
     }
}

quitcgi("You have to choose something to select by") unless grep {exists $param{$_}} keys %package_search_keys;


my $Archived = $param{archive} ? " Archived" : "";

my $this = munge_url('pkgreport.cgi?',
		      %param,
		     );

## Crude hack to deal with the thousands of
## pkgreport.cgi?submitter=jfb%40http://www.obslug in the logs.
## Why do they do this? Does this check here actually prevent anything?
quitcgi("Go away") if $this =~ /submitter=.*(http[%:]|\/|%2[fF])/;
## Better fix below ($spackage)
## http://lists.gnu.org/archive/html/help-debbugs/2016-10/msg00001.html
#quitcgi("Go away") if $this =~ /package=%/;

#open(DEBUG,"> /tmp/pkgreport.debug");
#print DEBUG $this;
#close(DEBUG);

my %indexentry;
my %strings = ();

my @bugs;

# addusers for source and binary packages being searched for
my $pkgsrc = getpkgsrc();
my $srcpkg = getsrcpkgs();
for my $package (# For binary packages, add the binary package
		 # and corresponding source package
		 make_list($param{package}||[]),
		 (map {defined $pkgsrc->{$_}?($pkgsrc->{$_}):()}
		  make_list($param{package}||[]),
		 ),
		 # For source packages, add the source package
		 # and corresponding binary packages
		 make_list($param{src}||[]),
		 (map {defined $srcpkg->{$_}?($srcpkg->{$_}):()}
		  make_list($param{src}||[]),
		 ),
		) {
     next unless defined $package;
     add_user($package.'@'.$config{usertag_package_domain},
	      \%ut,\%bugusertags,\%seen_users,\%cats,\%hidden)
	  if defined $config{usertag_package_domain};
}


# walk through the keys and make the right get_bugs query.

my $form_option_variables = {};
$form_option_variables->{search_key_order} = [@package_search_key_order];

# Set the title sanely and clean up parameters
my @title;
my @temp = @package_search_key_order;
while (my ($key,$value) = splice @temp, 0, 2) {
     next unless exists $param{$key};
     my @entries = ();
     for my $entry (make_list($param{$key})) {
	  # we'll handle newest below
	  next if $key =~ '^(newest|minbug|maxbug|page)$';
	  my $extra = '';
	  if (exists $param{dist} and ($key eq 'package' or $key eq 'src')) {
	       my %versions = get_versions(package => $entry,
					   (exists $param{dist}?(dist => $param{dist}):()),
					   (exists $param{arch}?(arch => $param{arch}):(arch => $config{default_architectures})),
					   ($key eq 'src'?(arch => q(source)):()),
					   no_source_arch => 1,
					   return_archs => 1,
					  );
	       my $verdesc;
	       if (keys %versions > 1) {
		    $verdesc = 'versions '. join(', ',
				    map { $_ .' ['.join(', ',
						    sort @{$versions{$_}}
						   ).']';
				   } keys %versions);
	       }
	       else {
		    $verdesc = 'version '.join(', ',
					       keys %versions
					      );
	       }
	       $extra= " ($verdesc)" if keys %versions;
	  }
	  if ($key eq 'maint' and $entry eq '') {
	       push @entries, "no one (packages without maintainers)"
	  }
	  else {
	       push @entries, $entry.$extra;
	  }
     }
     push @title,$value.' '.join(' or ', @entries) if @entries;
}

my $newest_bug = newest_bug();

## This must match the number in Pkgreport.pm.
my $max_bugs = $param{'max-bugs'} || 400;
$max_bugs = 400 if $max_bugs > 400;
my $warnmsg = '';

if (defined $param{minbug} || defined $param{maxbug}) {

    my $maxbug = $param{'maxbug'} || $newest_bug;
    my $minbug = $param{'minbug'} || 1;

    ## Some webbots (?) seem to type junk in here.
    quitcgi( "maxbug/minbug not an integer" )
      if $maxbug =~ /[^0-9]/ || $minbug =~ /[^0-9]/;

    quitcgi( "Too many bugs (maximum = $max_bugs)" )
        if $maxbug - $minbug > $max_bugs;

    @bugs = $minbug .. $maxbug;
    push @title, $minbug.' to '.$maxbug;
    $param{bugs} = [exists $param{bugs}?make_list($param{bugs}):(),
		    @bugs,
	];
}

if (defined $param{newest}) {

#    if ($param{newest} > $max_bugs) {
#        $warnmsg = "Too many bugs.  Only the newest $max_bugs are shown.\n";
#        $param{newest} = $max_bugs;
#    }

    quitcgi( "Expected an integer" ) if $param{newest} =~ /[^0-9]/;

    quitcgi( "Too many bugs (maximum = $max_bugs)" )
        if $param{newest} > $max_bugs;

     @bugs = ($newest_bug - $param{newest} + 1) .. $newest_bug;
     push @title, 'in '.@bugs.' newest reports';
     $param{bugs} = [exists $param{bugs}?make_list($param{bugs}):(),
		     @bugs,
		    ];
}

my $title = $gBugs.' '.join(' and ', map {/ or /?"($_)":$_} @title);
@title = ();

#yeah for magick!
@bugs = get_bugs((map {exists $param{$_}?($_,$param{$_}):()}
		  grep {$_ !~ '^(newest|minbug|maxbug|page)$'}
		  keys %package_search_keys, 'archive'),
		 usertags => \%ut,
		);

if ( exists $param{'base-order'} ) {

    if ( $bug_rev ) {
	## Newest first.
	@bugs = sort{$b<=>$a}(@bugs) ;
    } else {
	@bugs = sort{$a<=>$b}(@bugs) ;
    }
}

#open(DEBUG,"> /tmp/pkgreport.debug");
#print DEBUG join(', ', @bugs);
#close(DEBUG);

#open(DEBUG,"> /tmp/pkgreport3.debug");
#print DEBUG @$include;
#close(DEBUG);

# shove in bugs which affect this package if there is a package or a
# source given (by default), but no affects options given
if (not exists $param{affects} and not exists $param{noaffects} and
    (exists $param{source} or
     exists $param{package})) {
    push @bugs, get_bugs((map {exists $param{$_}?($_ =~ /^(?:package|source)$/?'affects':$_,$param{$_}):()}
			  grep {$_ ne 'newest'}
			  keys %package_search_keys, 'archive'),
			 usertags => \%ut,
			);
}

if (defined $param{version}) {
     $title .= " at version $param{version}";
}
elsif (defined $param{dist}) {
     $title .= " in $param{dist}";
}

$title = html_escape(hide_address($title));

my @names; my @prior; my @order;
determine_ordering(cats => \%cats,
		   param => \%param,
		   ordering => \$ordering,
		   names => \@names,
		   prior => \@prior,
		   title => \@title,
		   order => \@order,
		   sev_rev => $sev_rev,
		   pend_rev => $pend_rev,
		  );

# strip out duplicate bugs
my %bugs;
@bugs{@bugs} = @bugs;
@bugs = keys %bugs;


#open(DEBUG,"> /tmp/pkgreport.debug4");
#print DEBUG join(', ', @bugs);
#print DEBUG $tmp2arr->[0];
#close(DEBUG);

my $page = $param{page} || 1;

our $nbugs = 1;

my $result = pkg_htmlizebugs(bugs => \@bugs,
			     names => \@names,
			     title => \@title,
			     order => \@order,
			     prior => \@prior,
			     ordering => $ordering,
			     bugusertags => \%bugusertags,
			     bug_rev => $bug_rev,
			     bug_order => $bug_order,
			     repeatmerged => $param{repeatmerged},
			     include => $include,
			     exclude => $exclude,
			     this => $this,
			     options => \%param,
			     (exists $param{dist})?(dist => $param{dist}):(),
			     (exists $param{users})?(users => $param{users}):(),
			     page => $page,
			     max_bugs => $max_bugs,
			    );

my $this_nopage = $this;
$this_nopage =~ s/;?page=[0-9]*//g;

my $restricted = 0;

if ( $nbugs > $max_bugs )
{
    $restricted = 1;

    ## Bugs to show, inclusive, from 1.
    my $nmin = (($page-1) * $max_bugs) + 1;
    my $nmax = $nmin + $max_bugs - 1;
    $nmax = $nbugs if $nmax > $nbugs;

    my $nshow = $nmax - $nmin + 1;

    my $npages = int($nbugs/$max_bugs) + ( ($nbugs % $max_bugs) ? 1 : 0 );

    $warnmsg = "Showing $nshow of $nbugs bugs<br>\nPage: ";

    my $n;
    my $m;
    my $mmax = 10;

    for ( $n=1; $n <= $npages; $n++ )
    {
	$m = $n - $page;

	if ($m == 0)
	{
	    $warnmsg = "$warnmsg ${n}\n";
	}
	elsif ( $n == 1 || $n == $npages || abs($m) < $mmax )
	{
	    $warnmsg = "$warnmsg " .
		qq(<a href="$this_nopage;page=$n">) . "$n</a>";
	}
	elsif ( abs($m) == $mmax )
	{
	    $warnmsg = "$warnmsg ...\n";
	}
    }

    $warnmsg = "$warnmsg\n";
}


print "Content-Type: text/html; charset=utf-8\n\n";

print "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\">\n";
print "<HTML><HEAD>\n" . 
    "<TITLE>$title -- $gProject$Archived $gBug report logs</TITLE>\n" .
    qq(<link rel="stylesheet" href="$gWebHostBugDir/css/bugs.css" type="text/css">) .
    "\n" . qq(<meta name="robots" content="noindex, nofollow">) .
    "\n</HEAD>\n" .
    '<BODY onload="pagemain();">' .
    "\n";
print qq(<DIV id="status_mask"></DIV>\n);
print "<H1>" . "$gProject$Archived $gBug report logs: $title" .
      "</H1>\n";

## http://lists.gnu.org/archive/html/help-debbugs/2016-10/msg00001.html
## FIXME?
my $spackage = exists $param{package} ?
    html_escape(join(" ", make_list($param{package}))) : "";

print qq(\n<form method="GET" action="//${gCGIDomain}/pkgreport2.cgi">\n) .
  qq(<input type="hidden" name="_fo_combine_key_fo_searchkey_value_fo_searchvalue" value="1">\n) .
  qq(<input type="hidden" name="form_options" value="1">\n) .
  qq(<table><tr><th colspan="2">Select bugs</th>) .
  qq(<th colspan="2">Include Bugs</th><th></th></tr>\n) .
  qq(<tr><td><select name="_fo_searchkey">\n) .
  qq(<option value="package" selected>in package</option>\n) .
  qq(<option value="maint">in packages maintained by</option>\n) .
  qq(<option value="submitter">submitted by</option>\n) .
  qq(<option value="owner">owned by</option>\n) .
  qq(<option value="status">with status</option>\n) .
  qq(<option value="correspondent">with mail from</option>\n) .
  qq(<option value="newest">newest bugs</option>\n) .
  qq(</select></td>\n) .
  qq(<td><input type="text" name="_fo_searchvalue" value = "$spackage">\n) .
  qq(<input type="hidden" name="_fo_concatenate_into_include_fo_includekey_fo_includevalue" value="1"></td>\n) .
  qq(<td><select name="_fo_includekey">\n) .
  qq(<option value="subject">with subject containing</option>\n) .
  qq(<option value="tags">tagged</option>\n) .
  qq(<option value="severity">with severity</option>\n) .
  qq(<option value="pending">with pending state</option>\n) .
  qq(<option value="originator">with submitter containing</option>\n) .
  qq(<option value="forwarded">with forwarded containing</option>\n) .
  qq(<option value="owner">with owner containing</option>\n) .
  qq(<option value="package">with package</option>\n) .
  qq(</select></td>\n) .
  qq(<td><input type="text" name="_fo_includevalue" value =""></td>\n) .
  qq(<td><input type="submit" name="submit" value="Submit"></td></tr>\n) .
  qq(</table>\n</form>\n\n);

print qq(<p><a href="\#searchoptions">Options for current search</a>) .
  qq(&nbsp;&nbsp;&nbsp;&nbsp;) .
  qq(<a href="//${gWebDomain}/\#generalsearch">Advanced search</a></p>\n);

print "<strong>$warnmsg</strong>" if $warnmsg;

my $showresult = 1;

my $pkg = $param{package} if defined $param{package};
my $src = $param{src} if defined $param{src};

my $pseudodesc = getpseudodesc();
if (defined $pseudodesc and defined $pkg and exists $pseudodesc->{$pkg}) {
     delete $param{dist};
}

# output information about the packages

# for my $package (make_list($param{package}||[])) {
#      print generate_package_info(binary => 1,
# 				 package => $package,
# 				 options => \%param,
# 				 bugs    => \@bugs,
# 				);
# }
for my $package (make_list($param{src}||[])) {
     print generate_package_info(binary => 0,
				 package => $package,
				 options => \%param,
				 bugs    => \@bugs,
				);
}

if (exists $param{maint} or exists $param{maintenc}) {
    print "<p>Note that maintainers may use different Maintainer fields for\n";
    print "different packages, so there may be other reports filed under\n";
    print "different addresses.\n";
}
if (exists $param{submitter}) {
    print "<p>Note that people may use different email accounts for\n";
    print "different bugs, so there may be other reports filed under\n";
    print "different addresses.\n";
}

print qq(<p>Summary of bugs shown on this page of results:\n) if $restricted;

print $result;

print pkg_javascript() . "\n";

print "<hr><strong>$warnmsg</strong><hr>" if $warnmsg;

print qq(<h2 class="outstanding"><!--<a class="options" href="javascript:toggle(1)">-->Options<!--</a>--></h2><a name="searchoptions">\n);

print option_form(template => 'cgi/pkgreport_options',
		  param    => \%param,
		  form_options => $form_options,
		  variables => $form_option_variables,
		 );

print "<hr>\n";
print fill_in_template(template=>'html/html_tail',
		       hole_var => {'&strftime' => \&POSIX::strftime,
				   },
		      );
print "</body></html>\n";

