#!/bin/bash

renice 19 $$ >& /dev/null

indir=/var/lib/debbugs/spool/rrd

outdir=/var/www/rrd

[ -d $indir ] || exit 1

mkdir -p $outdir || exit 1

## Severities.
vars=( CR IM NO MI WI )
## red orange yellow yellow-green green
cols=( ff0000 ea8f00 eacc00 bfff00 3bff00 )

## Tags.
vars2=( wontfix patch moreinfo )
## red green blue
cols2=( cc3333 33cc33 3333cc  )


#span=( 1d 1w 1m 1y )
#span=( 1w 1y )
## It seems we are only keeping 5 years of data?
## Or we only started logging in 2011?
## See /usr/lib/debbugs/bug2rrd
span=( 1m 1y 5y )

tot="CDEF:tot=CR,IM,+,NO,+,MI,+,WI,+ VDEF:ltot=tot,LAST COMMENT:total GPRINT:ltot:%.0lf"

tot2="CDEF:tot=wontfix,patch,+,moreinfo,+ VDEF:ltot=tot,LAST COMMENT:total GPRINT:ltot:%.0lf"


mfile=/etc/debbugs/Maintainers

plist=`cut -f1 -d' ' $mfile | grep -Ev 'test|emacs21|emacs22|notemacs|aquamacs'`
#plist=emacs			# for testing

for package in $plist; do

    rrdfile=$indir/$package.rrd

    [ -e $rrdfile ] || continue

    stem=${rrdfile##*/}
    stem=${stem%.*}

    for (( n=0;n<${#vars[*]};n++ )); do

        v=${vars[$n]}
        c=${cols[$n]}

        d="DEF:$v=$rrdfile:$v:LAST AREA:${v}#$c:$v"
        [ $n -gt 0 ] && d=${d}:STACK
        def[$n]="$d VDEF:${v}last=${v},LAST GPRINT:${v}last:%.0lf"
    done

    for s in ${span[*]}; do

	## Must duplicate this below in other loop.
	case $s in
	    1m) xopt="--x-grid DAY:1:DAY:7:DAY:2:86400:%d" ;;
	    [3-9]y|??y) xopt="--x-grid MONTH:1:YEAR:1:YEAR:1:0:%Y" ;;
	    *) xopt= ;;
	esac

        png=${stem}_${s}.png
        ## If more than 500 bugs, switch to log scale.
        ## TODO really should be plotting on separate graphs anything
        ## which is less than 1/50 say (?) of the normal bugs.
#        logflag=`rrdtool lastupdate $rrdfile | \
#awk 'NR == 3 { for (i=2;i<=NF;i++) if ($i ~ /^[0-9]*$/) tot+=$i } \
#END {if (tot > 500) print "-o"}'`

        rrdtool graph -w 800 -h 200 $xopt --title "Bugs for $package" \
            $logflag --start -${s} $outdir/$png TEXTALIGN:left ${def[@]} \
	    $tot -W "Updated at `date -u`" \
            > /dev/null || exit 1

    done                        # $span


    rrdfile=$indir/${package}_tag.rrd

    [ -e $rrdfile ] || continue

    stem=${rrdfile##*/}
    stem=${stem%.*}

    for (( n=0;n<${#vars2[*]};n++ )); do

        v=${vars2[$n]}
        c=${cols2[$n]}

        d="DEF:$v=$rrdfile:$v:LAST AREA:${v}#$c:$v"
        [ $n -gt 0 ] && d=${d}:STACK
        def2[$n]="$d VDEF:${v}last=${v},LAST GPRINT:${v}last:%.0lf"
    done

    for s in ${span[*]}; do

	case $s in
	    1m) xopt="--x-grid DAY:1:DAY:7:DAY:2:86400:%d" ;;
	    [3-9]y|??y) xopt="--x-grid MONTH:1:YEAR:1:YEAR:1:0:%Y" ;;
	    *) xopt= ;;
	esac

        png=${stem}_${s}.png

        rrdtool graph -w 800 -h 200 $xopt --title "Tagged bugs for $package" \
            --start -${s} $outdir/$png TEXTALIGN:left ${def2[@]} \
	    $tot2 -l 0 -W "Updated at `date -u`" \
            > /dev/null || exit 1

    done                        # $span

done
